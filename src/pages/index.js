import React, { useState } from 'react'
import Footer from '../components/Footer/Footer';
import HeroSection from '../components/Hero/HeroSection';
import { homeObjOne, homeObjThree, homeObjTwo } from '../components/InfoSection/Data';
import Info from '../components/InfoSection/Info';
import Navbar from '../components/Navbar/Navbar'
import Services from '../components/Services/Services';
import Sidebar from '../components/Sidebar/Sidebar'

function Home() {
    const [isOpen,setIsOpen] = useState(false);

    const toggle = () => {
        setIsOpen(!isOpen);
    }
    return (
        <div>
            <Sidebar isOpen={isOpen} toggle={toggle}/>
            <Navbar toggle={toggle}/>
            <HeroSection />
            <Info {...homeObjOne}/>
            <Info {...homeObjTwo}/>
            <Services />
            <Info {...homeObjThree}/>
            <Footer />
        </div>
    )
}

export default Home
