import React, { useState } from "react";
import styled from "styled-components";
import { Button } from "../Button";

import videoBg from "../../videos/bg3.mp4";
import { MdArrowForward, MdKeyboardArrowRight } from "react-icons/md";
function HeroSection() {
	const [hover, setHover] = useState(false);

	const onHover = () => {
		setHover(!hover);
	};
	return (
		<>
			<HeroContainer>
				<HeroBg>
					<VideoBg autoPlay muted loop>
						<source src={videoBg} type="video/mp4" />
					</VideoBg>
				</HeroBg>
				<HeroContent>
					<HeroH1>Virtual Banking Made Easy</HeroH1>
					<HeroP>
						Sign ip for a new accon today and recive $250 in credit
						towards your next payment
					</HeroP>
					<HeroBtnWrapper>
						<Button
							onMouseEnter={onHover}
							onMouseLeave={onHover}
							primary="true"
							dark="true"
							to="signup"
							smooth={true}
							duration={500}
							spy={true}
							exact="true"
							offset={-80}
							activeClass="active"
						>
							Get started{" "}
							{hover ? <ArrowForward /> : <ArrowRight />}
						</Button>
					</HeroBtnWrapper>
				</HeroContent>
			</HeroContainer>
		</>
	);
}

const HeroContainer = styled.div`
	background: #0c0c0c;
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 0 30px;
	height: 100vh;
	position: relative;
	z-index: 1;
`;
const HeroBg = styled.div`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: rgba(0, 0, 0, 0.5);
`;
const VideoBg = styled.video`
	width: 100%;
	height: 100%;
	object-fit: cover;
`;
const HeroContent = styled.div`
	z-index: 3;
	max-width: 1200px;
	position: absolute;
	padding: 8px 24px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;
const HeroH1 = styled.h1`
	color: #fff;
	font-size: 48px;
	text-align: center;

	@media screen and (max-width: 768px) {
		font-size: 40px;
	}
	@media screen and (max-width: 480px) {
		font-size: 32px;
	}
`;
const HeroP = styled.p`
	margin-top: 24px;
	color: #fff;
	font-size: 24px;
	text-align: center;
	max-width: 600px;

	@media screen and (max-width: 768px) {
		font-size: 24px;
	}
	@media screen and (max-width: 480px) {
		font-size: 18px;
	}
`;
const HeroBtnWrapper = styled.div`
	margin-top: 32px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;
const ArrowForward = styled(MdArrowForward)`
	margin-left: 8px;
	font-size: 20px;
`;
const ArrowRight = styled(MdKeyboardArrowRight)`
	margin-left: 8px;
	font-size: 20px;
`;

export default HeroSection;
