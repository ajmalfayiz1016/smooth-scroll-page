import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Link as LinkR } from "react-router-dom";
import { Link as LinkS, animateScroll as scroll } from "react-scroll";
import { FaBars } from "react-icons/fa";
import { IconContext } from "react-icons/lib";

function Navbar({ toggle }) {
	const [scrollNav, setScrollNav] = useState(false);

	const changeNav = () => {
		if (window.scrollY >= 80) {
			setScrollNav(true);
		} else {
			setScrollNav(false);
		}
	};

	const toggleHome = () => {
		scroll.scrollToTop();
	};

	useEffect(() => {
		window.addEventListener("scroll", changeNav);
	}, []);

	return (
		<IconContext.Provider value={{ color: "#fff" }}>
			<Nav scrollNav={scrollNav}>
				<NavbarContainer>
					<NavLogo to="/" onClick={toggleHome}>
						Dolla
					</NavLogo>
					<MobileIcon onClick={toggle}>
						<FaBars />
					</MobileIcon>
					<NavMenu>
						<NavItem>
							<NavLink
								to="about"
								smooth={true}
								duration={500}
								spy={true}
								exact="true"
								offset={-80}
								activeClass="active"
							>
								About
							</NavLink>
						</NavItem>
						<NavItem>
							<NavLink
								to="discover"
								smooth={true}
								duration={500}
								spy={true}
								exact="true"
								offset={-80}
								activeClass="active"
							>
								Discover
							</NavLink>
						</NavItem>
						<NavItem>
							<NavLink
								to="services"
								smooth={true}
								duration={500}
								spy={true}
								exact="true"
								offset={-80}
								activeClass="active"
							>
								Services
							</NavLink>
						</NavItem>
						<NavItem>
							<NavLink
								to="signup"
								smooth={true}
								duration={500}
								spy={true}
								exact="true"
								offset={-80}
								activeClass="active"
							>
								Sign Up
							</NavLink>
						</NavItem>
					</NavMenu>
					<NavBtn>
						<NavBtnLink
							to="signin"
							smooth={true}
							duration={500}
							spy={true}
							exact="true"
							offset={-80}
							activeClass="active"
						>
							Sign In
						</NavBtnLink>
					</NavBtn>
				</NavbarContainer>
			</Nav>
		</IconContext.Provider>
	);
}

const Nav = styled.nav`
	background: ${({ scrollNav }) => (scrollNav ? "#000" : "transparent")};
	height: 80px;
	margin-top: -80px;
	display: flex;
	justify-content: center;
	align-items: center;
	font-size: 1rem;
	position: sticky;
	top: 0;
	transition: 0.3s all ease-in;
	z-index: 10;

	@media screen and (max-width: 960px) {
		transition: 0.8s all ease-in;
	}
`;

const NavbarContainer = styled.div`
	display: flex;
	justify-content: space-between;
	height: 80px;
	z-index: 1;
	width: 100%;
	padding: 0 24px;
	max-width: 1100px;
`;
const NavLogo = styled(LinkR)`
	font-size: 20px;
	justify-self: flex-start;
	cursor: pointer;
	font-size: 1.5rem;
	display: flex;
	align-items: center;
	margin-left: 24px;
	font-weight: bold;
	text-decoration: none;
	color: #fff;
`;
export const MobileIcon = styled.div`
	display: none;
	@media screen and (max-width: 768px) {
		display: block;
		position: absolute;
		top: 0;
		right: 0;
		transform: translate(-100%, 60%);
		font-size: 1.8rem;
		cursor: pointer;
		color: #fff;
	}
`;
export const NavMenu = styled.div`
	display: flex;
	align-items: center;
	list-style: none;
	text-align: center;
	margin-right: -22px;

	@media screen and (max-width: 768px) {
		display: none;
	}
`;
export const NavItem = styled.div`
	height: 80px;
`;
export const NavLink = styled(LinkS)`
	color: #fff;
	display: flex;
	align-items: center;
	text-decoration: none;
	padding: 0 1rem;
	height: 100%;
	cursor: pointer;

	&.active {
		border-bottom: 3px solid #01bf71;
	}
`;

const NavBtn = styled.nav`
	display: flex;
	align-items: center;

	@media screen and (max-width: 768px) {
		display: none;
	}
`;

const NavBtnLink = styled(LinkR)`
	border-radius: 50px;
	background: #01bf71;
	white-space: nowrap;
	padding: 10px 22px;
	color: #010606;
	font-size: 16px;
	outline: none;
	border: none;
	cursor: pointer;
	transition: all 0.2s ease-in-out;
	text-decoration: none;

	&:hover {
		transition: all 0.2s ease-in-out;
		background: #fff;
		color: #010606;
	}
`;

export default Navbar;
