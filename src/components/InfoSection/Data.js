export const homeObjOne = {
	id: "about",
	lightBg: false,
	lightText: true,
	lightTextDesc: true,
	topLine: "Premium Bank",
	headline: "Unlimited Transactionwith zero fees",
	description:
		"Get Access to our exclusive app that allows  you send unlimited transactiobs witout getting  charged any fees",
	buttonLabel: "Get started",
	imgStart: false,
	img: require("../../images/golf.svg").default,
	alt: "img",
	dark: true,
	primary: true,
	darkText: false,
};

export const homeObjTwo = {
	id: "discover",
	lightBg: true,
	lightText: false,
	lightTextDesc: false,
	topLine: "Premium Bank",
	headline: "Unlimited Transactionwith zero fees",
	description:
		"Get Access to our exclusive app that allows  you send unlimited transactiobs witout getting  charged any fees",
	buttonLabel: "Get started",
	imgStart: true,
	img: require("../../images/console.svg").default,
	alt: "img",
	dark: true,
	primary: true,
	darkText: true,
};

export const homeObjThree = {
	id: "signup",
	lightBg: false,
	lightText: true,
	lightTextDesc: true,
	topLine: "Premium Bank",
	headline: "Unlimited Transactionwith zero fees",
	description:
		"Get Access to our exclusive app that allows  you send unlimited transactiobs witout getting  charged any fees",
	buttonLabel: "Get started",
	imgStart: false,
	img: require("../../images/basket.svg").default,
	alt: "img",
	dark: true,
	primary: true,
	darkText: false,
};
